! Test runner program (eventually will be auto-generated)

! Copyright 2018 Steve Biggs

! This file is part of fortunit.

! fortunit is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! fortunit is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with fortunit.  If not, see http://www.gnu.org/licenses/.
program test_runner

    use test_case_test_test_was_run_mod

    implicit none

    ! Evenutally, this will loop over many tests
    type(test_case_test_test_was_run) :: test
    call test%run()

end program test_runner
