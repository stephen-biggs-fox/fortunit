! Records whether a test procedure was run

! Copyright 2018 Steve Biggs

! This file is part of fortunit.

! fortunit is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! fortunit is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with fortunit.  If not, see http://www.gnu.org/licenses/.
module was_run_mod

    use fortunit, only: test_case

    implicit none

    type, abstract, extends(test_case) ::  was_run
        logical :: was_run = .false.
    contains
        procedure :: test_procedure_to_be_run
    end type was_run


contains

    subroutine test_procedure_to_be_run(self)
        class(was_run) :: self
        self%was_run = .true.
    end subroutine test_procedure_to_be_run


end module was_run_mod
