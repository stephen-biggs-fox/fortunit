"""
Unit tests of FortunitModuleFinder

Copyright 2018 Stephen Biggs-Fox

This file is part of fortunit.

fortunit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fortunit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with fortunit.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from FortunitModuleFinder import FortunitModuleFinder


class testFortunitModuleFinder(TestCase):

    def setUp(self):
        self.file0 = 'file0.f90'
        self.file1 = 'file1.f90'
        self.file2 = 'file2.f90'
        self.files = [self.file0,
                      self.file1,
                      self.file2]
        self.finder = FortunitModuleFinder(self.files)

    @mock.patch('builtins.open', new_callable=mock.mock_open,
                read_data="program hello")
    def testNoFilesFoundWhenNoneContainUseFortunit(self, mo):
        handlers = (mo.return_value,
                    mock.mock_open(read_data="program hello").return_value,
                    mock.mock_open(read_data="program hello").return_value,)
        mo.side_effect = handlers
        files = self.finder.find()
        self.assertEqual([], files)

    @mock.patch('builtins.open', new_callable=mock.mock_open,
                read_data="program hello")
    def testOneFileFoundWhenOneContainsUseFortunit(self, mo):
        handlers = (mo.return_value,
                    mock.mock_open(read_data="use fortunit").return_value,
                    mock.mock_open(read_data="program hello").return_value,)
        mo.side_effect = handlers
        files = self.finder.find()
        self.assertEqual([self.file1], files)

    @mock.patch('builtins.open', new_callable=mock.mock_open,
                read_data="use fortunit")
    def testTwoeFilesFoundWhenTwoContainUseFortunit(self, mo):
        handlers = (mo.return_value,
                    mock.mock_open(read_data="prgram hello").return_value,
                    mock.mock_open(read_data="use fortunit").return_value,)
        mo.side_effect = handlers
        files = self.finder.find()
        self.assertEqual([self.file0, self.file2], files)

    @mock.patch('builtins.open', new_callable=mock.mock_open,
                read_data="use fortunit")
    def testAllFilesFoundWhenAllContainUseFortunit(self, mo):
        handlers = (mo.return_value,
                    mock.mock_open(read_data="use fortunit").return_value,
                    mock.mock_open(read_data="use fortunit").return_value,)
        mo.side_effect = handlers
        files = self.finder.find()
        self.assertEqual([self.file0, self.file1, self.file2], files)
