# fort_unit

An implementation of xUnit in Fortran

*NB: This project has only just started. As such, the product is not yet ready for use. Please check
back in due course for a working product.*


# Contributing

This project is not ready to accept collaborators yet. Please check back in due course to start
contributing.


# TODO list

- ~~Invoke test method~~
- Automatically subclass test classes
- Automatically update makefile
- Implement assertions
- Invoke setUp first
- Invoke tearDown afterward
- Invoke tearDown even if the test method fails
- Run multiple tests
- Report collected results
