# Makefile for fortunit

# Copyright 2018 Steve Biggs

# This file is part of fortunit.

# fortunit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.*/

# fortunit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with fortunit.  If not, see http://www.gnu.org/licenses/.*/


# Directories
# Source code of the framework itself
SRC = src
# Unit tests (source code) of the framework
TEST = test
# Build artefacts such as *.o and *.mod files
BUILD = build
# Compiled modules for installation
BIN = bin
# Unit test binaries for execution
TEST_BIN = test_bin

# Commonly used items
STD = f2008


# Source
$(BIN)/fortunit.mod: $(BUILD)/fortunit.mod $(BIN)/test_case_mod.mod
	cp $(BUILD)/fortunit.mod $(BIN)/fortunit.mod

$(BUILD)/fortunit.mod: $(BUILD)/fortunit.o

$(BUILD)/fortunit.o: $(BUILD)/test_case_mod.mod $(SRC)/fortunit.f90
	gfortran -std=$(STD) -o $(BUILD)/fortunit.o -J$(BUILD) -c $(SRC)/fortunit.f90

$(BIN)/test_case_mod.mod: $(BUILD)/test_case_mod.mod
	cp $(BUILD)/test_case_mod.mod $(BIN)/test_case_mod.mod

$(BUILD)/test_case_mod.mod: $(BUILD)/test_case.o

$(BUILD)/test_case.o: $(SRC)/test_case.f90
	gfortran -std=$(STD) -o $(BUILD)/test_case.o -J$(BUILD) -c $(SRC)/test_case.f90


# Tests
.PHONY: test
test: tests
	$(TEST_BIN)/test_runner

.PHONY: tests
tests: $(TEST_BIN)/test_runner

$(TEST_BIN)/test_runner: $(BUILD)/test_runner
	cp $(BUILD)/test_runner $(TEST_BIN)/test_runner

$(BUILD)/test_runner: $(BUILD)/test_case_test_test_was_run_mod.mod $(TEST)/test_runner.f90
	gfortran -std=$(STD) -o $(BUILD)/test_runner -J$(BUILD) $(BUILD)/test_case_test_test_was_run.o $(BUILD)/test_case_test.o $(BUILD)/test_case.o $(BUILD)/was_run_test_procedure_to_be_run.o $(BUILD)/was_run.o $(TEST)/test_runner.f90

$(BUILD)/test_case_test_test_was_run_mod.mod: $(BUILD)/test_case_test_test_was_run.o

$(BUILD)/test_case_test_test_was_run.o: $(BUILD)/test_case_test_mod.mod $(TEST)/test_case_test_test_was_run.f90
	gfortran -std=$(STD) -o $(BUILD)/test_case_test_test_was_run.o -J$(BUILD) -c $(TEST)/test_case_test_test_was_run.f90

$(BUILD)/test_case_test_mod.mod: $(BUILD)/test_case_test.o

$(BUILD)/test_case_test.o: $(BUILD)/fortunit.mod $(BUILD)/test_case_mod.mod $(BUILD)/was_run_test_procedure_to_be_run_mod.mod $(TEST)/test_case_test.f90
	gfortran -std=$(STD) -o $(BUILD)/test_case_test.o -J$(BUILD) -c $(TEST)/test_case_test.f90

$(BUILD)/was_run_test_procedure_to_be_run_mod.mod: $(BUILD)/was_run_test_procedure_to_be_run.o

$(BUILD)/was_run_test_procedure_to_be_run.o: $(BUILD)/was_run_mod.mod $(TEST)/was_run_test_procedure_to_be_run.f90
	gfortran -std=$(STD) -o $(BUILD)/was_run_test_procedure_to_be_run.o -J$(BUILD) -c $(TEST)/was_run_test_procedure_to_be_run.f90

$(BUILD)/was_run_mod.mod: $(BUILD)/was_run.o

$(BUILD)/was_run.o: $(BUILD)/fortunit.mod $(TEST)/was_run.f90
	gfortran -std=$(STD) -o $(BUILD)/was_run.o -J$(BUILD) -c $(TEST)/was_run.f90


# Clean
.PHONY: clean
clean:
	rm -rf $(BUILD)/* $(BIN)/* $(TEST_BIN)/*
