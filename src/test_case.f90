! An xunit test case to be extended when writing tests

! Copyright 2018 Steve Biggs

! This file is part of fortunit.

! fortunit is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! fortunit is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with fortunit.  If not, see http://www.gnu.org/licenses/.
module test_case_mod

    implicit none

    type, abstract :: test_case
    contains
        procedure :: run
        procedure(abstract_test_procedure), deferred :: test_procedure
    end type test_case


    abstract interface
        subroutine abstract_test_procedure(self)
            import :: test_case
            class(test_case) :: self
        end subroutine abstract_test_procedure
    end interface


contains

    subroutine run(self)
        class(test_case) :: self
        call self%test_procedure()
    end subroutine run


end module test_case_mod
