! Main module of fortunit to be used by test modules

! Copyright 2018 Steve Biggs

! This file is part of fortunit.

! fortunit is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! fortunit is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with fortunit.  If not, see http://www.gnu.org/licenses/.
module fortunit
  use test_case_mod
end module fortunit
